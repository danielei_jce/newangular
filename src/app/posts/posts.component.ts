import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'my-posts',
  templateUrl: './posts.component.html',
  styles: [`
        .posts li { cursor: default; }
        .posts li:hover { background: #ecf0f1; }
        .list-group-item.active, 
        .list-group-item.active:hover { 
          background-color: #ecf0f1;
          border-color: #ecf0f1; 
          color: #2c3e50;
         }
   `]
})
export class PostsComponent implements OnInit {
posts;
currentPost;
isLoading:boolean=true;

select(post){
  this.currentPost=post;
}
  constructor(private _postService:PostsService) { }

deletePost(post){
  this.posts.splice(this.posts.indexOf(post),1)
}
editPost(post){
    this.posts.splice(this.posts.indexOf(post),1,post)
    console.log(this.posts);
  }
addPost(post){
  this.posts.push(post);
}
  ngOnInit() {
    this._postService.getPosts().subscribe(postsData=>{
      this.posts=postsData;
      this.isLoading=false
    });
  }

}
