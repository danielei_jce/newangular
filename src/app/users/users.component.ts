import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'my-users',
  templateUrl: './users.component.html',
  styles: [`
        .users li { cursor: default; }
        .users li:hover { background: #ecf0f1; }
        .list-group-item.active, 
        .list-group-item.active:hover { 
          background-color: #ecf0f1;
          border-color: #ecf0f1; 
          color: #2c3e50;
         }
   `]
 })
export class UsersComponent implements OnInit {
users;
currentUser;
isLoading:boolean=true;

  constructor(private _userService:UsersService) { }

  select(user){
		this.currentUser = user; 
 }
  ngOnInit() {
    //this.users= this._userService.getUsers();
    this._userService.getUsers().subscribe(usersData=>
      {
        this.users=usersData;
        this.isLoading=false
      }); 
  }
  deleteUser(user){
    this.users.splice(
      this.users.indexOf(user),1)
  }
  addUser(user){
    this.users.push(user);
  }

}
