import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class UsersService {

  private _url="http://jsonplaceholder.typicode.com/users";

constructor(private _http:Http) { }
 
 getUsers(){
   return this._http.get(this._url).map(res=>res.json()).delay(500);
   //go to the server and leaves an observable obj
   //the result comes back as string - we change it to json
   //the casting performed in arrow notation when "map" doing it
 }
}

