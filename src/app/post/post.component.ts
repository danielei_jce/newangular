import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {Post} from './post';
@Component({
  selector: 'my-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})

export class PostComponent implements OnInit {

  @Output() deleteEvent=new EventEmitter<Post>();
  @Output() editEvent=new EventEmitter<Post>();

  post:Post;
  originId:string;
  originTitle:string;
  isEdit:boolean=false;
  undoFlag:boolean=false;
  editButtonText="Edit";

  constructor() { }

  toggleEdit(){
    this.isEdit=!this.isEdit;
    if(this.isEdit){
       this.editButtonText="Save"
       this.undoFlag=true
       this.editEvent.emit(this.post)
    }
    else
      this.editButtonText="Edit"
      this.undoFlag=false
   }
   toggleUndo(){
    this.undoFlag=true
    this.post.id=this.originId
    this.post.title=this.originTitle
    this.isEdit=false
    this.editButtonText="Edit"
  }
  sendDelete(){
    this.deleteEvent.emit(this.post)
  }
  ngOnInit() {
    this.originId=this.post.id;
    this.originTitle=this.post.title;
  }

}
